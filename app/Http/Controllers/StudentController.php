<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Student;
use Validator;

class StudentController extends Controller
{
    
     //@index
    public function index(){

       $this->data['Student'] = Student::all();
       $this->data['list'] = true;

       return view('student',$this->data); 
    }

    public function create(Request $request){

    	if($_POST){

    		$this->validate($request , [
	            'name'      =>  'required',
	            'img'      =>  'required|mimes:jpeg,png,jpg,gif,svg',
	            'email'      =>  'required|email',
	            'mobile'      =>  'required',
	            'dob'      =>  'required',
	            'regno'      =>  'required|unique:student',
	        ]);


	        //Image
	         $image = $request->file('img');
	         if(!$image = ''){
	                $image = $request->file('img')->getClientOriginalName();
	                $size= $request->file('img')->getSize();
	                $name = 'img'.rand(111,999).$request->file('img')->getClientOriginalName();
	                $image = 'uploads/certificate/'.$name;
	                $request->file('img')->move("uploads/certificate/", $name);
	        }else {
	                $image = 'upload/certificate/demo.jpg';
	        
	        }

	        $Student = Student::create();
	        $Student->name = $request->name;
	        $Student->photo = $image;
	        $Student->email = $request->email;
	        $Student->mobile = $request->mobile;
	        $Student->dob = $request->dob;
	        $Student->regno = $request->regno;
	        $Student->save();


	        if($Student){
               return redirect('/')->with('message','Register Successfully');
	        }else{
	            return redirect('/')->with('message','Sorry, Something went wrong please try again');
	        }
    	

    	}

    	$this->data['add'] = true;
       return view('student',$this->data);

    }

    public function update(Request $request,$id){

    	if($_POST){

    		$this->validate($request , [
	            'name'      =>  'required',
	            'email'      =>  'required|email',
	            'mobile'      =>  'required',
	            'dob'      =>  'required',
	            'regno'      =>  'required|unique:student',
	        ]);


            $image = $request->file('img');
             if(!isset($image))
            {
                $image = 'upload/certificate/demo.jpg';
            }else{


                request()->validate([
                        'img' => 'image|mimes:jpeg,png,jpg,gif,svg',
                    ]);

                    $image = $request->file('img');
                    if(!$image = ''){
                        $image = $request->file('img')->getClientOriginalName();
                        $size= $request->file('img')->getSize();
                        $name = 'img'.rand(111,999).$request->file('img')->getClientOriginalName();
                        $image = 'uploads/certificate/'.$name;
                        $request->file('img')->move("uploads/certificate/", $name);
                    }else {
                        $image = 'upload/certificate/demo.jpg';
                
                    }

            }

	        $Student = Student::find($id);
	        $Student->name = $request->name;
	        $Student->photo = $image;
	        $Student->email = $request->email;
	        $Student->mobile = $request->mobile;
	        $Student->dob = $request->dob;
	        $Student->regno = $request->regno;
	        $Student->save();
            
            return redirect('/')->with('message','Updated Successfully');	
    	}

    	$this->data['edit'] = true;
    	$this->data['Student'] =Student::where('id',$id)->first();
    	return view('student',$this->data);
    	
    }

 public function read(Request $request,$id){

       
        $this->data['read'] = true;
        $this->data['Student'] =Student::where('id',$id)->first();
        return view('student',$this->data);
        
    }

    public function delete($id){

    	$isDeleted = Student::where('id',$id)->delete();
    
         if($isDeleted){
            return redirect('/')->with('message','Deleted Successfully');
        }else{
            return redirect('/')->with('message','Sorry, Record not found!');
        }
    	
    }

}
