<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Student;
use Validator,Redirect,Response,File;

class StudentController extends Controller
{
    //
      //@index
    public function index(){

       $Student = Student::all();
      
       return response()->json([
				  'success'=>true,
				  'data'=>$Student,
				 ],200);
    }

    public function create(Request $request){

             $validator = Validator::make($request->only('name','photo','email','mobile','dob','regno'),
	         [
	            'name'      =>  'required',
	            'photo'      =>  'required|mimes:jpeg,png,jpg,gif,svg',
	            'email'      =>  'required|email',
	            'mobile'      =>  'required',
	            'dob'      =>  'required',
	            'regno'      =>  'required|unique:student',
	         ]);
    		

	        if($validator->fails())
	        {
	             return response()->json([
	                 "success"=>false,
	                 "message"=>$validator->messages()->toArray(),
	             ],200);
	        }
           
            if ($files = $request->file('photo')) {
            	//store file into document folder
                $file = $request->file('photo')->store('public/student/profile');
                $Student = Student::create();
		        $Student->name = $request->name;
		        $Student->photo = $file;
		        $Student->email = $request->email;
		        $Student->mobile = $request->mobile;
		        $Student->dob = $request->dob;
		        $Student->regno = $request->regno;
		        $Student->save();

		        return response()->json([
				  'success'=>true,
				  'data'=>$Student,
				 ],200);
            }

	        
         

    }

    public function update(Request $request){

    	$validator = Validator::make($request->only('id','name','photo','email','mobile','dob','regno'),
	         [
	         	'id'        => 'required',
	            'name'      =>  'required',
	            'photo'      =>  'required|mimes:jpeg,png,jpg,gif,svg',
	            'email'      =>  'required|email',
	            'mobile'      =>  'required',
	            'dob'      =>  'required',
	            'regno'      =>  'required',
	         ]);


	        if($validator->fails())
	        {
	             return response()->json([
	                 "success"=>false,
	                 "message"=>$validator->messages()->toArray(),
	             ],200);
	        }
           
            if ($files = $request->file('photo')) {
            	//store file into document folder
                $file = $request->file('photo')->store('public/student/profile');
                $Student = Student::find($request->id);;
		        $Student->name = $request->name;
		        $Student->photo = $file;
		        $Student->email = $request->email;
		        $Student->mobile = $request->mobile;
		        $Student->dob = $request->dob;
		        $Student->regno = $request->regno;
		        $Student->save();

		        return response()->json([
				  'success'=>true,
				  'data'=>$Student,
				 ],200);
            }
    	
    }

    public function delete(Request $request){


	    $validator = Validator::make($request->only('id'),
	         [
	         	'id'        => 'required',
	         ]);

	    if($validator->fails())
        {
             return response()->json([
                 "success"=>false,
                 "message"=>$validator->messages()->toArray(),
             ],200);
        }

    	$isDeleted = Student::where('id',$request->id)->delete();
    	if($isDeleted){
            return response()->json([
				  'success'=>true,
				  'message'=>'deleted Successfully',
				 ],200);
    	}else{
    		return response()->json([
				  'success'=>false,
				  'message'=>'deleted Successfully',
				 ],200);

    	}
    }
}
