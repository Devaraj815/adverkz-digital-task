
   @extends('layouts.master')
@section('content')







            



                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">




 <div class="page-header">
<div class="row align-items-end">
<div class="col-lg-8">
<div class="page-header-title">
<div class="d-inline">
<h4>Student Registeration </h4>
<span>Here you can Register student details </span>
</div>
</div>
</div>
</div>
</div>

  @if(session()->has('message'))
                   <div class="alert alert-success">
                      {{ session()->get('message') }}
                   </div>


@endif

<!-- vaild error show -->
@foreach ($errors->all() as $error)
   <div class="alert alert-danger">
                {{ $error }}

              </div>
 @endforeach


@if(isset($list))


         <div class="page-body">
            <div class="row">
                                        
<div class="col-sm-12">

 <div class="card">
<div class="card-header">
<div class="row">
  
<div class="col-md-6">
  
<h5>Student Details</h5>



</div>

  <div class="col-md-6 text-right">

                 <a href="{{url('student/add')}}"> <button type="button" class="btn btn-primary m-b-0">Add</button></a>
                    </div>


</div>






</div>
<div class="card-block">
<div class="dt-responsive table-responsive">
<table id="simpletable" class="table table-striped table-bordered nowrap">
 <thead>

                          <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Reg.No</th>
                            <th>Mobile No</th>
                            <th>DOB</th>
                            <th>Action</th>
                           
                          </tr>
                        </thead>
                        <tbody>
    <?php $count = 1; if(isset($Student) && !empty($Student)){ ?>
            @foreach($Student as $s)
                          
                          <tr>
                                                  <td>{{ $count }}</td>
                            <td>{{$s->name }}</td>
                            <td>{{$s->regno}}</td>
                           
<td>
     {{$s->mobile}}                     
 </td>
 <td>
    {{$s->dob}}
 </td>
                              
                              <td >
                                    <a href="{{url('student/read')}}/{{ $s->id }}" class="btn btn-success"><i class="fa fa-eye"></i> Read</a>
                                 <a href="{{url('student/edit')}}/{{ $s->id }}" class="btn btn-warning"><i class="fa fa-pencil-square-o"></i>Edit</a>
                        <a href="{{url('student/delete')}}/{{ $s->id }}" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a>
                    

                             

                            </td>
                            
                          </tr>

                              <?php $count++ ?>
            @endforeach
        <?php } ?>
                    
                
                        
                        </tbody>

</table>
</div>
</div>
</div>

</div>


                                        </div>
                                    </div>


@endif


@if(isset($add))

<form action="{{url('student/add')}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}

<div class="page-body">
    <div class="row">
        <div class="row col-sm-12">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                      


                          



                        <div class="form-group row">

                          <div class="col-md-6">
                            <label class="col-sm-12 col-form-label"><b>Name</b></label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control"  name="name" placeholder="Enter Student Name" required>
                            </div>
</div>

                       <div class="col-md-6">
                            <label class="col-sm-12 col-form-label"><b>Reg.No</b></label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control"  name="regno" placeholder="Enter Student Reg.no"  required>
                            </div>
</div>



                        </div>

                          <div class="form-group row">

                          <div class="col-md-6">
                            <label class="col-sm-12 col-form-label"><b>DOB</b></label>
                            <div class="col-sm-12">
                                <input type="date" class="form-control"  name="dob" required>
                            </div>
</div>

                       <div class="col-md-6">
                            <label class="col-sm-12 col-form-label"><b>Mobile No</b></label>
                            <div class="col-sm-12">
                                <input type="number" class="form-control"  name="mobile" placeholder="Enter Student Mobile no"   required>
                            </div>
</div>



                        </div>

                        <div class="form-group row">

                          <div class="col-md-6">
                            <label class="col-sm-12 col-form-label"><b>Email id</b></label>
                            <div class="col-sm-12">
                                <input type="email" class="form-control"  name="email"  placeholder="Enter Student Email" required>
                            </div>
</div>

                       <div class="col-md-6">
                            <label class="col-sm-12 col-form-label"><b>Student Photo</b></label>
                            <div class="col-sm-12">
                                <input type="file" class="form-control"  name="img"   required>
                            </div>
</div>



                        </div>


                        

                          <button type="submit" class="btn btn-primary m-b-0">Save</button>

                       








 






                    </div>
                </div>
            </div>
        </div>
     
    </div>
</div>
</form>


@endif


@if(isset($edit))


<form action="{{url('student/edit')}}/{{ $Student->id }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}

<div class="page-body">
    <div class="row">
        <div class="row col-sm-12">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                      


                          



                        <div class="form-group row">

                          <div class="col-md-6">
                            <label class="col-sm-12 col-form-label"><b>Name</b></label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control"  name="name" value="{{ $Student->name }}" placeholder="Enter Student Name" required>
                            </div>
</div>

                       <div class="col-md-6">
                            <label class="col-sm-12 col-form-label"><b>Reg.No</b></label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control"  name="regno" value="{{ $Student->regno }}" placeholder="Enter Student Reg.no"  required>
                            </div>
</div>



                        </div>

                          <div class="form-group row">

                          <div class="col-md-6">
                            <label class="col-sm-12 col-form-label"><b>DOB</b></label>
                            <div class="col-sm-12">
                                <input type="date" class="form-control"  name="dob" value="{{ $Student->dob }}" required>
                            </div>
</div>

                       <div class="col-md-6">
                            <label class="col-sm-12 col-form-label"><b>Mobile No</b></label>
                            <div class="col-sm-12">
                                <input type="number" class="form-control"  name="mobile" value="{{ $Student->mobile }}" placeholder="Enter Student Mobile no"   required>
                            </div>
</div>



                        </div>

                        <div class="form-group row">

                          <div class="col-md-6">
                            <label class="col-sm-12 col-form-label"><b>Email id</b></label>
                            <div class="col-sm-12">
                                <input type="email" class="form-control"  name="email" value="{{ $Student->email }}" placeholder="Enter Student Email" required>
                            </div>
</div>

                       <div class="col-md-6">
                            <label class="col-sm-12 col-form-label"><b>Student Photo</b></label>
                            <div class="col-sm-12">
                                <input type="file" class="form-control"  name="img" value="{{ $Student->img }}"  required>
                            </div>
</div>



                        </div>


                        

                          <button type="submit" class="btn btn-primary m-b-0">Save</button>

                       








 






                    </div>
                </div>
            </div>
        </div>
     
    </div>
</div>
</form>


@endif


@if(isset($read))



<div class="page-body">
    <div class="row">
        <div class="row col-sm-12">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                      


                          



                        <div class="form-group row">

                          <div class="col-md-6">
                            <label class="col-sm-12 col-form-label"><b>Name</b></label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control"  name="name" placeholder="Enter Student Name" required>
                            </div>
</div>

                       <div class="col-md-6">
                            <label class="col-sm-12 col-form-label"><b>Reg.No</b></label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control"  name="regno" placeholder="Enter Student Reg.no"  required>
                            </div>
</div>



                        </div>

                          <div class="form-group row">

                          <div class="col-md-6">
                            <label class="col-sm-12 col-form-label"><b>DOB</b></label>
                            <div class="col-sm-12">
                                <input type="date" class="form-control"  name="dob" required>
                            </div>
</div>

                       <div class="col-md-6">
                            <label class="col-sm-12 col-form-label"><b>Mobile No</b></label>
                            <div class="col-sm-12">
                                <input type="number" class="form-control"  name="mobile" placeholder="Enter Student Mobile no"   required>
                            </div>
</div>



                        </div>

                        <div class="form-group row">

                          <div class="col-md-6">
                            <label class="col-sm-12 col-form-label"><b>Email id</b></label>
                            <div class="col-sm-12">
                                <input type="email" class="form-control"  name="email"  placeholder="Enter Student Email" required>
                            </div>
</div>

                       <div class="col-md-6">
                            <label class="col-sm-12 col-form-label"><b>Student Photo</b></label>
                            <div class="col-sm-12">
                                <input type="file" class="form-control"  name="img"   required>
                            </div>
</div>



                        </div>


                        

                       <a href="{{url('/')}}">   <button class="btn btn-primary m-b-0">back</button>

                       </a>

                       








 






                    </div>
                </div>
            </div>
        </div>
     
    </div>
</div>

@endif




                                </div>

                            </div>
                        </div>
                    </div>


        @endsection
